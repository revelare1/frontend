
import type { CustomThemeConfig } from '@skeletonlabs/tw-plugin';

export const revelareTheme: CustomThemeConfig = {
    name: 'revelare-theme',
    properties: {
        // =~= Theme Properties =~=
        "--theme-font-family-base": `system-ui`,
        "--theme-font-family-heading": `system-ui`,
        "--theme-font-color-base": "0 0 0",
        "--theme-font-color-dark": "255 255 255",
        "--theme-rounded-base": "9999px",
        "--theme-rounded-container": "8px",
        "--theme-border-base": "1px",
        // =~= Theme On-X Colors =~=
        "--on-primary": "0 0 0",
        "--on-secondary": "255 255 255",
        "--on-tertiary": "255 255 255",
        "--on-success": "255 255 255",
        "--on-warning": "0 0 0",
        "--on-error": "0 0 0",
        "--on-surface": "0 0 0",
        // =~= Theme Colors  =~=
        // primary | #CD591F
        "--color-primary-50": "248 230 221", // #f8e6dd
        "--color-primary-100": "245 222 210", // #f5ded2
        "--color-primary-200": "243 214 199", // #f3d6c7
        "--color-primary-300": "235 189 165", // #ebbda5
        "--color-primary-400": "220 139 98", // #dc8b62
        "--color-primary-500": "205 89 31", // #CD591F
        "--color-primary-600": "185 80 28", // #b9501c
        "--color-primary-700": "154 67 23", // #9a4317
        "--color-primary-800": "123 53 19", // #7b3513
        "--color-primary-900": "100 44 15", // #642c0f
        // secondary | #5A0B4D
        "--color-secondary-50": "230 218 228", // #e6dae4
        "--color-secondary-100": "222 206 219", // #decedb
        "--color-secondary-200": "214 194 211", // #d6c2d3
        "--color-secondary-300": "189 157 184", // #bd9db8
        "--color-secondary-400": "140 84 130", // #8c5482
        "--color-secondary-500": "90 11 77", // #5A0B4D
        "--color-secondary-600": "81 10 69", // #510a45
        "--color-secondary-700": "68 8 58", // #44083a
        "--color-secondary-800": "54 7 46", // #36072e
        "--color-secondary-900": "44 5 38", // #2c0526
        // tertiary | #767676
        "--color-tertiary-50": "234 234 234", // #eaeaea
        "--color-tertiary-100": "228 228 228", // #e4e4e4
        "--color-tertiary-200": "221 221 221", // #dddddd
        "--color-tertiary-300": "200 200 200", // #c8c8c8
        "--color-tertiary-400": "159 159 159", // #9f9f9f
        "--color-tertiary-500": "118 118 118", // #767676
        "--color-tertiary-600": "106 106 106", // #6a6a6a
        "--color-tertiary-700": "89 89 89", // #595959
        "--color-tertiary-800": "71 71 71", // #474747
        "--color-tertiary-900": "58 58 58", // #3a3a3a
        // success | #4B5842
        "--color-success-50": "228 230 227", // #e4e6e3
        "--color-success-100": "219 222 217", // #dbded9
        "--color-success-200": "210 213 208", // #d2d5d0
        "--color-success-300": "183 188 179", // #b7bcb3
        "--color-success-400": "129 138 123", // #818a7b
        "--color-success-500": "75 88 66", // #4B5842
        "--color-success-600": "68 79 59", // #444f3b
        "--color-success-700": "56 66 50", // #384232
        "--color-success-800": "45 53 40", // #2d3528
        "--color-success-900": "37 43 32", // #252b20
        // warning | #CEFF1A
        "--color-warning-50": "248 255 221", // #f8ffdd
        "--color-warning-100": "245 255 209", // #f5ffd1
        "--color-warning-200": "243 255 198", // #f3ffc6
        "--color-warning-300": "235 255 163", // #ebffa3
        "--color-warning-400": "221 255 95", // #ddff5f
        "--color-warning-500": "206 255 26", // #CEFF1A
        "--color-warning-600": "185 230 23", // #b9e617
        "--color-warning-700": "155 191 20", // #9bbf14
        "--color-warning-800": "124 153 16", // #7c9910
        "--color-warning-900": "101 125 13", // #657d0d
        // error | #DE4D86
        "--color-error-50": "250 228 237", // #fae4ed
        "--color-error-100": "248 219 231", // #f8dbe7
        "--color-error-200": "247 211 225", // #f7d3e1
        "--color-error-300": "242 184 207", // #f2b8cf
        "--color-error-400": "232 130 170", // #e882aa
        "--color-error-500": "222 77 134", // #DE4D86
        "--color-error-600": "200 69 121", // #c84579
        "--color-error-700": "167 58 101", // #a73a65
        "--color-error-800": "133 46 80", // #852e50
        "--color-error-900": "109 38 66", // #6d2642
        // surface | #d9d9d9
        "--color-surface-50": "249 249 249", // #f9f9f9
        "--color-surface-100": "247 247 247", // #f7f7f7
        "--color-surface-200": "246 246 246", // #f6f6f6
        "--color-surface-300": "240 240 240", // #f0f0f0
        "--color-surface-400": "228 228 228", // #e4e4e4
        "--color-surface-500": "217 217 217", // #d9d9d9
        "--color-surface-600": "195 195 195", // #c3c3c3
        "--color-surface-700": "163 163 163", // #a3a3a3
        "--color-surface-800": "130 130 130", // #828282
        "--color-surface-900": "106 106 106", // #6a6a6a

    }
}