export type Summary = {
  title: string;
  url: string;
  summary: string;
  tags: string[];
}


export const Summarize = async (title: string, url: string, content?: string, tags?: string[]) => {
  const response = await fetch('http://127.0.0.1:8000/summarize', {
    method: 'POST',
    body:  JSON.stringify({title: title, url: url, content: content, tags: tags}),
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  });
  const myJson = await response.json(); //extract JSON from the http response
  return myJson;
}


export const Search = async (query: string) => {
  const response = await fetch('http://127.0.0.1:8000/search?query='+query, {
    method: 'GET',
  });
  const myJson = await response.json(); //extract JSON from the http response
  return myJson;
}


export const Delete = async (id: number) => {
  const response = await fetch('http://127.0.0.1:8000/summaries/'+id+'/delete', {
    method: 'DELETE',
  });
}


export const Retrieve = async (id: number): Promise<Summary> => {
  const response = await fetch('http://127.0.0.1:8000/summaries/'+id, {
    method: 'GET',
  });
  const myJson = await response.json(); //extract JSON from the http response
  return myJson;
}


export const Retrieve_all = async (): Promise<Summary[]> => {
  const response = await fetch('http://127.0.0.1:8000/summaries', {
    method: 'GET'
  });
  return await response.json();
}


export const Add_tags = async (id: number, tags: string[]) => {
  const response = await fetch('http://127.0.0.1:8000/summaries'+id+'/add_tags', {
    method: 'POST',
    body:  JSON.stringify({tags: tags}),
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  });
  const myJson = await response.json(); //extract JSON from the http response
  return myJson;
}


export const Remove_tags = async (id: number, tags: string[]) => {
  const response = await fetch('http://127.0.0.1:8000/summaries'+id+'/remove_tags', {
    method: 'DELETE',
    body:  JSON.stringify({tags: tags}),
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  });
  const myJson = await response.json(); //extract JSON from the http response
  return myJson;
}